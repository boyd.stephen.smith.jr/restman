{-# language OverloadedStrings #-}

module UI
    ( startUI
    ) where

-- brick
import Brick.AttrMap (AttrMap)
import qualified Brick.AttrMap as Brick
import Brick.Focus (focusRing, focusSetCurrent)
import Brick.Main (App(..), defaultMain)
import Brick.Types (EventM)
import Brick.Widgets.Border (borderAttr)
import Brick.Widgets.Edit (editor)
import Brick.Widgets.List (listSelectedFocusedAttr)

-- bytestring
import qualified Data.ByteString.Lazy as Lazy (ByteString)

-- text
import Data.Text (Text)

-- vty
import Graphics.Vty.Attributes
  ( Attr(Attr, attrBackColor, attrForeColor, attrStyle, attrURL)
  , MaybeDefault(Default, SetTo)
  , currentAttr
  , reverseVideo
  )

-- local libs
import HTTP.Client (Header, UseDefaultHeaders)
import Lib (chooseCursor, draw, handleEvent)
import Types (AppS(..))


-- | Empty attribute map
attrMap :: AppS -> AttrMap
attrMap _ =
  Brick.attrMap currentAttr
    [ (listSelectedFocusedAttr, currentAttr {attrStyle = SetTo reverseVideo})
    , (borderAttr, termDefaults)
    ]
 where
  termDefaults = Attr
    { attrStyle = Default
    , attrForeColor = Default
    , attrBackColor = Default
    , attrURL = Default
    }

{-|
As the poor name implies, not sure what to call the library entry point, or really even what it
should look like at this point.
-}
startUI
  :: String -- ^ Initial HTTP method
  -> UseDefaultHeaders -- ^ how to handle default headers
  -> [Header] -- ^ additional or replacement headers
  -> Maybe Lazy.ByteString -- ^ payload or Nothing
  -> IO ()
startUI method useDefault custHeaders payloadLbs = do
  _ <- defaultMain app initialState
  pure ()
 where
  app = App
    { appDraw = draw
    , appChooseCursor = chooseCursor
    , appHandleEvent = handleEvent
    , appStartEvent = pure ()
    , appAttrMap = attrMap
    }
  initialState = AppS
    { focus = focusSetCurrent urlEditorName $ focusRing [urlEditorName, methodEditorName]
    , methodEditor = editor methodEditorName (Just 1) method
    , overlayState = Nothing
    , urlEditor = editor urlEditorName (Just 1) ""
    , lastResponse = ""
    , useDefaultHeaders = toEnum $ fromEnum useDefault
    , customHeaders = custHeaders
    , payload = payloadLbs
    }
  methodEditorName = "methodEditor"
  urlEditorName = "urlEditor"


{-# language DeriveGeneric #-}
module TUI where

-- ghc
import GHC.Generics (Generic)

-- text
import Data.Text as T

-- wreq
import Network.Wreq (Options)

-- Types of HTTP requests, extensible with Custom
data Verb = GET | HEAD | POST | PUT | DELETE | OPTIONS | PATCH | Custom T.Text
  deriving (Show, Eq, Generic)

-- Resource Type for making requets
data ReqConfig =
  ReqConfig
    { _httpUri :: T.Text
    , _options :: Options
    , _verb :: Verb
    } deriving (Show, Generic)
